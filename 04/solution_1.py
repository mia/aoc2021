#!/usr/bin/env python3
with open("input.txt", "r") as _input:
    seq = [int(n) for n in _input.readline().rstrip().split(",")]
    boards = []

    for line in _input.readlines():
        if len(line.rstrip()) == 0:
            boards.append([])
        else:
            boards[-1].append([int(n) for n in line.split()])

for num in seq:
    for board in boards:
        def calcscore():
            for row in board:
                if num in row:
                    row[row.index(num)] = False
                if row == [False] * len(row):
                    return True
            for column in zip(*board[::-1]):
                if list(column) == [False] * len(column):
                    return True

        if (calcscore()):
            score = num * sum([sum(row) for row in board])

    if "score" in locals():
        print("Answer:", score)
        break
