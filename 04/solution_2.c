#include <stdio.h>

unsigned score(signed char (*board)[5]) {
    unsigned sum = 0;

    for (unsigned r = 0; r < 5; r++) {
        for (unsigned c = 0; c < 5; c++) {
            if (board[r][c] > 0)
                sum += board[r][c];
        }
    }

    return sum;
}

int main(void) {
    FILE *fp = fopen("input.txt", "r");
    signed char seq[100], boards[100][5][5];
    unsigned i, b, r, c, lastwin;

    for (i = 0; i < 100; i++) {
        fscanf(fp, "%hhi%*c", &seq[i]);
    }

    for (b = 0; b < 100; b++) {
        for (r = 0; r < 5; r++) {
            for (c = 0; c < 5; c++)
            fscanf(fp, "%hhi", &boards[b][r][c]);
        }
    }

    for (i = 0; i < 100; i++) {
        for (b = 0; b < 100; b++) {
            if (boards[b][0][0] == -2)
                continue;

            for (r = 0; r < 5; r++) {
                int rowscore = 0;
                for (c = 0; c < 5; c++) {
                    if (boards[b][r][c] == seq[i])
                        boards[b][r][c] = -1;

                    rowscore += boards[b][r][c];
                }

                if (rowscore == -5)
                    goto win;
            }

            for (c = 0; c < 5; c++) {
                int colscore = 0;
                for (r = 0; r < 5; r++) {
                    colscore += boards[b][r][c];
                }

                if (colscore == -5)
                    goto win;
            }
            continue;
win:
            lastwin = score(boards[b]) * seq[i];
            boards[b][0][0] = -2;
        }
    }

    printf("Answer: %u\n", lastwin);
}
