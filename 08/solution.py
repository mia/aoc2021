#!/usr/bin/env python3

import sys

known = {2:1,3:7,4:4,7:8}

silver=0
gold=0

for l in sys.stdin.readlines():
    i, o = [x.split() for x in l.rstrip().split(" | ")]

    digits = {}
    for d in i:
        k = known.get(len(d))
        if k:
            digits[k] = d

    bcdf = set(digits[4])
    cf = set(digits.get(7) or digits[1])
    eg = set("abcdefg") - cf - bcdf
    bd = bcdf - cf
    out = []

    for d in o:
        l = len(d)
        d = set(d)
        hasbd = len(d & bd) == 2
        haseg = len(d & eg) == 2
        if l in known:
            silver += 1
            out.append(known[l])
        elif l == 5:
            if hasbd:
                out.append(5)
            elif haseg:
                out.append(2)
            else:
                out.append(3)
        elif l == 6:
            if haseg:
                if hasbd:
                    out.append(6)
                else:
                    out.append(0)
            else:
                out.append(9)

    gold += int("".join([str(i) for i in out]))

print(f"Silver: {silver}\nGold: {gold}")
