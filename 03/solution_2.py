#!/usr/bin/env python3
from collections import Counter

def flt(data, most):
    positions = len(data[0])
    for i in range(positions):
        cnt = Counter(list(zip(*data[::-1]))[i]).most_common(2)
        if len(cnt) > 1:
            if cnt[0][1] == cnt[1][1]:
                cond = '1' if most else '0'
            else:
                cond = cnt[0 if most else 1][0]
            data = [x for x in data if x[i] == cond]
            if len(data) == 1:
                break
    return data[0]

with open("input.txt", "r") as _input:
    data = [l.rstrip() for l in _input.readlines()]

print("Answer:", int(flt(data, True), 2) * int(flt(data, False), 2))
