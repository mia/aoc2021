#!/usr/bin/env python3
from collections import Counter

with open("input.txt", "r") as _input:
    most_common_bits = [Counter(col).most_common(2) for col in zip(*[l.rstrip() for l in _input.readlines()][::-1])]

gamma, epsilon = [int("".join(x)[::-1], 2) for x in zip(*[(x[0], y[0]) for x, y in most_common_bits][::-1])]

print("Answer:", gamma * epsilon)
