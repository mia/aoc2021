#!/usr/bin/env python3

import sys
import numpy
import networkx

def solve(cave):
    G = networkx.DiGraph()
    for i, row in enumerate(cave):
        for j, v in enumerate(row):
            if 0 <= i-1:
                G.add_edge((i-1, j), (i, j), weight=v)
            if 0 <= j-1:
                G.add_edge((i, j-1), (i, j), weight=v)
            if i+1 < len(cave):
                G.add_edge((i+1, j), (i, j), weight=v)
            if j+1 < len(cave):
                G.add_edge((i, j+1), (i, j), weight=v)

    return networkx.dijkstra_path_length(G, (0,0), (len(cave)-1,len(cave[0])-1))

cave = numpy.genfromtxt(sys.stdin, dtype=int, delimiter=1)

print("Silver:", solve(cave))

largecave = numpy.tile(cave, (5,5))

for i in range(1,5):
    largecave[i*len(cave):]+=1
    largecave[:,i*len(cave):]+=1

largecave-=1
largecave%=9
largecave+=1

print("Gold:", solve(largecave))
