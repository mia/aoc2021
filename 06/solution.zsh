#!/usr/bin/zsh
typeset -A F; for i ({0..9}) F+=($i 0)
for v (${(@s.,.)$(<input.txt)}) ((F[$v]++))
calc() {
    for ((i=$2;i<=$3;i++)) ((F[$[(i+7)%9]]+=$F[$[i%9]]))
    print Solution $1: $((${(j:+:)F}))
}
calc 1 1 79
calc 2 80 255
