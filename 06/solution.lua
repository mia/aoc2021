#!/usr/bin/env lua

fish = {}
for i = 0, 8 do fish[i] = 0 end

function count_fish()
    count = 0
    for fi = 0, 8 do
        count = count + fish[fi]
    end
    return count
end

for line in io.lines("input.txt") do
    for n in string.gmatch(line, "([^,]+)") do
        fish[tonumber(n)] = fish[tonumber(n)] + 1
    end

    for iter = 1, 256 do
        f = fish[0]
        for fi = 0, 7 do
            fish[fi] = fish[fi+1]
        end
        fish[8] = f
        fish[6] = fish[6] + f

        if iter == 80 then
            print("Solution 1:", count_fish())
        end
    end

    print("Solution 2:", count_fish())
end
