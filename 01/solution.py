#!/usr/bin/env python3

with open("input.txt", "r") as _input:
    data = [int(l) for l in _input.readlines()]

sliding_sums = [sum(v) for v in zip(data, data[1:], data[2:])]
answer = [b > a for a, b in zip(sliding_sums, sliding_sums[1:])].count(True)

print(f"Answer: {answer}")
