#include <stdio.h>

enum {winsize=3};

int main(void) {
    int num[winsize], res[4]={0}, new, i, j;

    for (i=0;scanf("%d",&num[i%winsize])==1;i++) {
        if (i) {
            if (num[i%winsize] > res[1])
                res[0]++;

            res[1] = num[i%winsize];

            if (i>2) {
                for (j=0,new=0;j<winsize;j++)
                    new += num[j];

                if (new > res[3])
                    res[2]++;

                res[3] = new;
            }
        }
    }

    printf("Silver: %d\nGold: %d\n", res[0], res[2]);
}
