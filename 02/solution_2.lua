#!/usr/bin/env lua

aim = 0
h = 0
v = 0

function down(val) aim = aim + val end
function up(val) aim = aim - val end
function forward(val) h = h + val; v = v + aim * val end

for line in io.lines("input.txt") do
    cmd, val = string.match(line, "(%S+) (%S+)")
    _G[cmd](val)
end

print(string.format("Answer: %d", h * v))
