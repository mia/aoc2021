#!/usr/bin/env python3
from collections import defaultdict
d = defaultdict(int)

with open("input.txt", "r") as _input:
    for i, n in [x.split(' ') for x in _input.readlines()]:
        d[i] += int(n)

print("Answer:", (d["down"] - d["up"]) * d["forward"])
