#!/usr/bin/zsh

integer h=0
integer v=0

IFS="
"
for line in "${(@f)}"$(<input.txt)
{
    cmd=(${(s/ /)line})
    case $cmd[1] in
        down)
            ((v += $cmd[2]))
            ;;
        up)
            ((v -= $cmd[2]))
            ;;
        forward)
            ((h += $cmd[2]))
            ;;
    esac
}

print Answer: $((h * v))
