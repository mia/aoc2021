#!/usr/bin/zsh

integer aim=0
integer h=0
integer v=0

IFS="
"
for line in "${(@f)}"$(<input.txt)
{
    cmd=(${(s/ /)line})
    case $cmd[1] in
        down)
            ((aim += $cmd[2]))
            ;;
        up)
            ((aim -= $cmd[2]))
            ;;
        forward)
            ((h += $cmd[2]))
            ((v += aim * $cmd[2]))
            ;;
    esac
}

print Answer: $((h * v))
