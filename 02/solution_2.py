#!/usr/bin/env python3

aim = 0
h = 0
v = 0

with open("input.txt", "r") as _input:
    for i, n in [x.split(' ') for x in _input.readlines()]:
        n = int(n)
        if i == "down": aim += n
        elif i == "up": aim -= n
        elif i == "forward": h += n; v += aim * n

print("Answer:", h * v)
