#!/usr/bin/env lua

h = 0
v = 0

function down(val) v = v + val end
function up(val) v = v - val end
function forward(val) h = h + val end

for line in io.lines("input.txt") do
    cmd, val = string.match(line, "(%S+) (%S+)")
    _G[cmd](val)
end

print(string.format("Answer: %d", h * v))
