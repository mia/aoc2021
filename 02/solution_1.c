#include <stdio.h>
#include <string.h>

int main(void) {
    FILE *fp = fopen("input.txt", "r");
    char buf[16] = {0};
    unsigned n, h = 0, v = 0;

    while (fgets(buf, 16, fp) != NULL) {
        sscanf(buf, "%*s %u", &n);

        switch (buf[0]) {
            case 'd':
                v += n;
                break;
            case 'u':
                v -= n;
                break;
            case 'f':
                h += n;
                break;
        }
    }

    printf("Answer: %u\n", h * v);
}
