#include <stdio.h>
#define put(x, y) intersections += ++map[x][y] == 2

int main(void) {
    FILE *fp = fopen("input.txt", "r");
    unsigned char map[1000][1000] = {0};
    unsigned x0, x1, y0, y1;
    unsigned intersections = 0;

    while (fscanf(fp, "%u,%u -> %u,%u", &x0, &y0, &x1, &y1) == 4) {
        char inc = 1;
        if (x0 == x1) {
            if (y0 > y1)
                inc = -1;
            for (unsigned y = y0; y != y1; y += inc)
                put(x0, y);
            put(x0, y1);
        } else if (y0 == y1) {
            if (x0 > x1)
                inc = -1;
            for (unsigned x = x0; x != x1; x += inc)
                put(x, y0);
            put(x1, y0);
        }
    }

    printf("Answer: %d\n", intersections);
}
