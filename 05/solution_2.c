#include <stdio.h>
#define put(x, y) intersections += ++map[x][y] == 2

int main(void) {
    FILE *fp = fopen("input.txt", "r");
    unsigned char map[1000][1000] = {0};
    unsigned x0, x1, y0, y1;
    unsigned intersections = 0;

    while (fscanf(fp, "%u,%u -> %u,%u", &x0, &y0, &x1, &y1) == 4) {
        char incY = y0 < y1 ? 1 : -1;
        char incX = x0 < x1 ? 1 : -1;
        if (x0 == x1) {
            for (unsigned y = y0; y != y1; y += incY)
                put(x0, y);
            put(x0, y1);
        } else if (y0 == y1) {
            for (unsigned x = x0; x != x1; x += incX)
                put(x, y0);
            put(x1, y0);
        } else {
            unsigned x = x0, y = y0;
            while (x != x1 && y != y1) {
                put(x, y);
                x += incX;
                y += incY;
            }
            put(x, y);
        }
    }

    printf("Answer: %d\n", intersections);
}
