#!/usr/bin/env lua

map = {}

for line in io.lines("input.txt")  do
    x0, y0, x1, y1 = string.match(line, "(%d+),(%d+) .+ (%d+),(%d+)")
    x0 = tonumber(x0) x1 = tonumber(x1) y0 = tonumber(y0) y1 = tonumber(y1)

    if x0 == x1 then
        map[x0] = map[x0] or {}
        if y0 > y1 then step = -1 else step = 1 end
        for y = y0, y1, step do
            map[x0][y] = (map[x0][y] or 0) + 1
        end
    elseif y0 == y1 then
        if x0 > x1 then step = -1 else step = 1 end
        for x = x0, x1, step do
            map[x] = map[x] or {}
            map[x][y0] = (map[x][y0] or 0) + 1
        end
    end
end

intersections = 0
for _, col in pairs(map) do
    for _, f in pairs(col) do
        if f > 1 then
            intersections = intersections + 1
        end
    end
end

print("Answer:", intersections)
